﻿using System;
using System.Linq;

namespace HackerRank
{
    class Program
    {
        static void Main(String[] args) {
        }
    }

    class DiagonalDifference
    {
        static void Answer(String[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            int[][] a = new int[n][];
            for (int a_i = 0; a_i < n; a_i++)
            {
                string[] a_temp = Console.ReadLine().Split(' ');
                a[a_i] = Array.ConvertAll(a_temp, Int32.Parse);
            }

            int sumD1 = 0,
                sumD2 = 0,
                count = n - 1;

            for (int i = 0; i < n; i++)
            {
                sumD1 += a[i][i];
                sumD2 += a[count--][i];
            }

            Console.WriteLine(Math.Abs(sumD1 - sumD2));
        }
    }

    class CompareTheTriplets
    {
        static void Answer(String[] args)
        {
            string[] tokens_a0 = Console.ReadLine().Split(' ');
            int a0 = Convert.ToInt32(tokens_a0[0]);
            int a1 = Convert.ToInt32(tokens_a0[1]);
            int a2 = Convert.ToInt32(tokens_a0[2]);
            string[] tokens_b0 = Console.ReadLine().Split(' ');
            int b0 = Convert.ToInt32(tokens_b0[0]);
            int b1 = Convert.ToInt32(tokens_b0[1]);
            int b2 = Convert.ToInt32(tokens_b0[2]);

            int[]   a = { a0, a1, a2 },
                    b = { b0, b1, b2 };
            int aCount = 0,
                bCount = 0;

            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] > b[i])
                {
                    aCount += 1;
                }
                if (a[i] < b[i])
                {
                    bCount += 1;
                }
            }

            int[] result = { aCount, bCount };
            Console.WriteLine(String.Join(" ", result));
        }
    }

    class aVeryBigSum
    {
        static void Answer(String[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            string[] ar_temp = Console.ReadLine().Split(' ');
            long[] ar = Array.ConvertAll(ar_temp, Int64.Parse);

            long result = 0;
            for (int i = 0; i < n; i++)
            {
                result += ar[i];
            }

            Console.WriteLine(result);
        }
    }

    class PlusMinus
    {
        static void Answer(String[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            string[] arr_temp = Console.ReadLine().Split(' ');
            int[] arr = Array.ConvertAll(arr_temp, Int32.Parse);

            double pos = 0,
                neg = 0,
                zer = 0;

            for (int i = 0; i < n; i++)
            {
                if (arr[i] > 0)
                {
                    pos++;
                }
                else
                {
                    if (arr[i] < 0)
                    {
                        neg++;
                    }
                    else
                    {
                        if (arr[i] == 0)
                        {
                            zer++;
                        }
                    }
                }
            }

            double posFrac = 0,
                    negFrac = 0,
                    zerFrac = 0;

            posFrac = pos / n;
            negFrac = neg / n;
            zerFrac = zer / n;

            Console.WriteLine(Math.Round(posFrac, 6));
            Console.WriteLine(Math.Round(negFrac, 6));
            Console.WriteLine(Math.Round(zerFrac, 6));
        }
    }

    class Staircase
    {
        static void Answer(String[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());

            int spaces = n - 1;

            for (int i = 1; i <= n; i++)
            {
                string str = "";
                string hashes = str.PadLeft(i, '#');
                Console.WriteLine(hashes.PadLeft(n));
                spaces--;
            }
        }
    }

    class MiniMaxSum
    {
        static void Answer(String[] args)
        {
            string[] arr_temp = Console.ReadLine().Split(' ');
            int[] arr = Array.ConvertAll(arr_temp, Int32.Parse);

            Array.Sort(arr);

            int len = arr.Length - 1,
                count = 0;
            long lowSum = 0,
                 highSum = 0;

            for (int i = 0; i < arr.Length; i++)
            {
                if (count < 4)
                {
                    lowSum += arr[i];
                    highSum += arr[len--];
                }
                count++;
            }

            Console.WriteLine(lowSum.ToString() + " " + highSum.ToString());
        }
    }

    /*
     * Colleen is turning n years old! Therefore, she has n candles of various heights on her cake, and candle i has heighti. 
     * Because the taller candles tower over the shorter ones, Colleen can only blow out the tallest candles.
     * Given the heighti for each individual candle, find and print the number of candles she can successfully blow out.
     * Input Format
     * The first line contains a single integer, n, denoting the number of candles on the cake. 
     * The second line contains n space-separated integers, where each integer i describes the height of candle i.
     * Constraints
     * 1 <= n <= 10^5
     * 1 <= i <= 10^7
     * Output Format
     * Print the number of candles Colleen blows out on a new line.
     * Sample Input 0
     * 4
     * 3 2 1 3
     * Sample Output 0
     * 2
     * Explanation 0
     * We have one candle of height 1, one candle of height 2, and two candles of height 3. 
     * Colleen only blows out the tallest candles, meaning the candles where heighti = 3. 
     * Because there are 2 such candles, we print 2 on a new line.
     */
    class BirthdayCakeCandles
    {
        static void Answer(String[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            string[] ar_temp = Console.ReadLine().Split(' ');
            int[] ar = Array.ConvertAll(ar_temp, Int32.Parse);

            int maxVal = ar.Max();
            Console.WriteLine(Array.FindAll(ar, x => x == maxVal).Length);
        }
    }

    /*
     * Note: 
     * Midnight is 12:00:00AM on a 12-hour clock, and 00:00:00 on a 24-hour clock. 
     * Noon is 12:00:00AM on a 12-hour clock, and 12:00:00 on a 24-hour clock.
     * Input Format
     * A single string containing a 12-hour clock format
     * Output Format
     * A single string containing a 24-hour clock format
     * Sample Input
     * 07:05:45PM
     * Sample Output
     * 19:05:45
     */
    class TimeConversion
    {
        static void Answer(String[] args)
        {
            string s = Console.ReadLine();
            DateTime result = DateTime.Parse(s);
            Console.WriteLine(result.TimeOfDay);
        }
    }
}
